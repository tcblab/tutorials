# GROMACS tutorials #

- Internal tutorial database for collecting tutorials that will be made available at seperate places later.
- Do not share this git repository with the outside world. 
- New tutorial are added by copying `template-and-how-to`.
- Have a look at `resources` for useful tips and tricks.

# Install Jupyter using virtual environment in Linux

Install `python-virtualenv` python packet manager(in some distributions called `virtualenv`):

`sudo apt-get install virtualenv`

Create a virtual environment:

`virtualenv -p /usr/bin/python3 venv3`

where `-p` is a path to python executable, `venv3` - is a virtual environment folder.

Activate virtual environment:

`source venv3/bin/activate`

Install jupyter:

`pip install jupyter`

Run jupyter:

`jupyter notebook`
