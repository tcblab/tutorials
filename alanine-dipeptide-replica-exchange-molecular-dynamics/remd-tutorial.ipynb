{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# REMD simulations in GROMACS\n",
    "\n",
    "## Overview\n",
    "\n",
    "This tutorial comes in two parts. In the first, the theory behind replica exchange simulations will be briefly described. Then, we will look at how to perform temperature REMD on a small peptide in explicit solvent.\n",
    "\n",
    "## Theory of replica exchange simulations\n",
    "\n",
    "Many molecular simulation scenarios require ergodic sampling of energy landscapes that feature many minima. Often the barriers between minima can be difficult to cross at ambient temperatures over accessible simulation time scales. This means that results are often confounded by the choice of initial conditions, because the simulation never leaves\n",
    "that space.\n",
    "\n",
    "Replica exchange simulations seek to enhance the sampling in such scenarios by running numerous independent replicas in slightly different ensembles, and periodically exchanging the coordinates of replicas between the ensembles. Typically, the set \n",
    "\n",
    "of replicas is constructed so that one extreme of the set of replicas is the ensemble from which sampling is wanted, and the other extreme is one where barriers can be crossed more easily. So, if\n",
    "\n",
    "* the attempts to exchange replicas between ensemble produce valid ensembles,\n",
    "* the probability of exchange attempts succeeding is high enough, and\n",
    "* the resulting flow of replicas over the ensembles produces good mixing,\n",
    "\n",
    "then the resulting sampling will be statistically correct and closer to the ergodic ideal.\n",
    "\n",
    "How can this work? It relies on doing Monte Carlo moves of replicas between ensembles. The probability of observing a replica in a particular state depends on the potential energy and the temperature, i.e.\n",
    "\n",
    "$$P(x) \\propto \\exp^{-\\frac{U(x)}{k_B T}}$$.\n",
    "\n",
    "The range of potential energies that is observed follows a normal distribution (because of the Central Limit Theorem). If two ensembles are chosen so that the set of states have significant overlap in the resulting potential energy distributions, then when we observe a state in one ensemble there is appreciable chance that it could have been observed in the other.\n",
    "\n",
    "![Potential energy distributions of alanine dipeptide replicas.](combined-pot.jpg)\n",
    "\n",
    "This allows us to construct a Metropolis-style Monte Carlo move that proposes that we exchange the coordinates found in two replicas based upon the probabilities that they would have been observed in the two ensembles. The exchange is accepted only if a random number has a suitable value. When done correctly, detailed balance is achieved, and the sampling in both ensembles is correct whether or not an exchange took place.\n",
    "\n",
    "What range of ensembles should be chosen? The method of REMD (also known as parallel tempering) conducts the same simulation over a range of temperatures. We don't have to choose temperatures, but it's convenient and easy to understand. The highest temperature is chosen so that the barriers will be crossed. Replicas wander up and down through temperature space as exchanges are accepted. Barriers are crossed probabilistically at all of the temperatures, but at a higher rate at the higher temperatures. Those states filter down to the lower temperatures if they \"belong\" to the corresponding ensembles. In this way, the ergodicity of the sampling is enhanced.\n",
    "\n",
    "For more details, see for example the many papers by Sugita Okamoto and Ulrich Hansmann.\n",
    "\n",
    "## Stage 1\n",
    "\n",
    "### Prerequisites for this notebook\n",
    "\n",
    "This notebook needs to run in an environment that will find an MPI-enabled version of GROMACS, e.g. by running `source /opt/gromacs-2018.1-mpi/bin/GMXRC` before starting Jupyter. Note that the default installation of GROMACS does not have MPI enabled, and REMD simulations require the use of MPI, so if you are running this tutorial on your own cluster, you might need to get that installed, first.\n",
    "\n",
    "### Planning an REMD simulation\n",
    "\n",
    "There are a number of inter-connected issues to consider in designing an REMD simulation:\n",
    "\n",
    "* What range of temperatures do I want to span?\n",
    "* How many replicas will I use?\n",
    "* What exchange probability should I seek?\n",
    "* How much compute resource can I use?\n",
    "* How will I generate my starting coordinates for each replica?\n",
    "\n",
    "In this tutorial, these decisions are made arbitrarily, but for real studies you will want to consult the literature for guidance here. Suggested reading includes papers by David Kofke, Ulrich Hansmann, and Mark Abraham.\n",
    "\n",
    "Temperatures should often be distributed across the replicas in a geometric progression, i.e. with the same ratio used to scale each temperature from the one below it. If the energy landscapes are sufficiently similar across the temperature space, then this will keep the overlap of the potential energy distributions constant. In turn, this will keep the probability of accepting exchanges constant. Beware, though - in real simulations you will find bottle-necks in temperature space that restrict replica flow. You should be prepared to revise your temperature scheme after some testing.\n",
    "\n",
    "As you can read in the above literature, an exchange acceptance probability around 0.2 is generally a good idea. Often you will have to experiment with the number of replicas you need to use to span the desired temperature range to achieve about that exchange probability. If you are simulating in the NPT ensemble, you might try this [REMD temperature generator](http://folding.bmc.uu.se/remd/) to help out.\n",
    "\n",
    "Here, we will use only four replicas, so that the tutorial can run reasonably. Beware that you might want many hundreds of replicas to span a decent temperature range if you were looking at something like a large protein! The temperatures we will use are 298.00, 308.00, 318.34, and 329.02.\n",
    "\n",
    "### Preparing REMD equilibrations in GROMACS\n",
    "\n",
    "Normally, `gmx mdrun` runs a single simulation. For temperature REMD we need to run a number of simulations that can communicate. This is done via the `gmx_mpi mdrun -multidir` mechanism, which runs `N` simulations within the same program. This requires an MPI-enabled version of GROMACS (usually installed as `gmx_mpi mdrun`), and `N` input `.tpr` files, each running at a different temperature. Naturally, we will have to equilibrate those to their target temperature before starting replica exchange, and that is most convenient to do with `gmx_mpi mdrun -multidir`!\n",
    "\n",
    "You will find directories `equil0`, `equil1`, ... `equil3` inside the `stage1` directory. Each of those contains all the files needed to equilibrate a simulation at the right temperature. You can check the contents of the `.mdp` files with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stage1/equil0/equil.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "or if you want to check the reference temperatures for the thermostats in each, you can do that efficiently with a shell wildcard in a command like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep ref-t stage1/equil*/equil.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want to run grompp in each directory to build the independent `.tpr` files we need. To do the first one we might chain together some bash commands so that we use `cd` to change directories and then call `gmx grompp` like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!(cd stage1/equil0; gmx_mpi grompp -f equil -c confout)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That gets tedious when you have a lot of directories! Since we're humans, we learn to use tools to do boring things for us. The [bash](http://en.wikipedia.org/wiki/Bash_(Unix_shell)) shell lets you write a nice loop to do that operation. Here's how that looks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!(for directory in stage1/equil[0123] ; do (cd $directory; gmx_mpi grompp -f equil -c confout); done)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This says to loop over `equil0` ... `equil3` directories, and for each of those, change into the directory and run `gmx grompp`. This can be a tremendous time saver. Now if you do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls stage1/equil*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "you will see that there is a `topol.tpr` file in each directory. That's what we need to run `mdrun` for the equilibration, so now we're ready to roll! Run the simulation with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mpirun -np 4 gmx_mpi mdrun -multidir stage1/equil[0123]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Perhaps you will need `mdrun_mpi` rather than `gmx_mpi mdrun` to run this on some clusters, depending how your system administrators built GROMACS.) Use `ls` again, and you will see that each of those directories now has a `md.log` and `ener.edr` file corresponding to the run you just did."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls stage1/equil*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `.mdp` file you just used only runs a few steps, because you might not have time to wait for an equilibration to actually run on your machine. If you want to do a proper equilibration, edit all the `equil.mdp` files to set `nsteps = 100000`. This will run 200 ps of equilibration, so go and have coffee or something while you wait! For this tutorial though, the result of an equilibration is already available to use in the next stage!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stage 2\n",
    "\n",
    "### Running a REMD simulation in GROMACS\n",
    "\n",
    "In this stage, we'll take the output from an equilibration run of decent length and use that as input for the REMD simulation proper. All the files for the production simulation are found in the `stage2` directory. In each `sim0` ... `sim3` sub-directory, you will see the usual collection of input files, plus the `.cpt` file that is the CheckPoinT file from the previous equilibration run. The `sim.mdp` input file in each directory has a different temperature, and is set up to expect to continue the matching equilibration run. We can see that difference if we run commands like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!diff stage1/equil2/equil.mdp stage2/sim2/sim.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, you can see that we are running a continuation, rather than generating new velocities that would only need equilibrating again! That the only differences between the simulation `.mdp` files are in the reference temperatures can be seen with commands like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!diff stage2/sim[12]/sim.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's use `gmx grompp` to prepare the input `.tpr` files, again using a `bash` loop to do the monkey work for us"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!(for directory in stage2/sim[0123]; do (cd $directory; gmx_mpi grompp -f sim -c confout -t equil-state); done)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time we told `gmx grompp` to use the state from the equilibration `.cpt` file. OK, it's time to run the REMD simulation with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mpirun -np 4 gmx_mpi mdrun -multidir stage2/sim[0123] -replex 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This run will take a bit longer than the equilibration run, but is still only a \"toy\" run. We triggered the use of REMD with the `-replex` flag, which also specified that we will take 100 MD integration steps that should take place between exchange attempts. There's a bit of disagreement in the literature about how long that should be. Waiting around 1 ps seems wise in view of how long it takes to make an independent observation of the potential energy, so with a 2 fs time step in the `.mdp` file, around 500 steps between exchanges will be OK. Above, we did exchanges more often, just for the purposes of getting things to happen during the tutorial.\n",
    "\n",
    "So, what happened? Let's look at one of the four `.log` files that were written, with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stage2/sim1/md.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and searching through it for the output peculiar to replica exchange. Fortunately, all of that is prefixed with the same string for replica exchange, so we can search for that with `Repl`.\n",
    "\n",
    "You can see that `gmx_mpi mdrun` did some checking that you gave it a consistent set of input `.tpr` files. Further on you might see output like\n",
    "\n",
    "    Replica exchange at step 100 time 0.20000\n",
    "    Repl 0 <-> 1  dE_term =  1.880e+00 (kT)\n",
    "    Repl ex  0    1    2    3\n",
    "    Repl pr   .15       .22\n",
    "\n",
    "\n",
    "Here we see that at step 100 (which was the first attempt), the time was 0.2 ps. The potential energy of the state in ensemble 1 was about 1.88 kT higher than that of the state in ensemble 0, which meant that an exchange would only succeed about 15% of the time. Since this is the `md.log` file for simulation 1, we can't see the details for the exchange attempt between simulations 2 and 3, but if we went one of to their `md.log` files, we would see those details.\n",
    "\n",
    "The exchange attempt at step 3500 in my run was a bit different (yours will be different again)\n",
    "\n",
    "    Replica exchange at step 3500 time 7.00000\n",
    "    Repl 0 <-> 1  dE_term = -7.012e-01 (kT)\n",
    "    Repl ex  0 x  1    2 x  3\n",
    "    Repl pr   1.0       1.0\n",
    "\n",
    "At replica-exchange steps, GROMACS alternates between two disjoint sets of replica pairs, so that each replica attempts an exchange with both of its neighbours once every two attempts. This time, replica 1 had a potential energy that was lower than replica 0, so the exchange is certain to succeed. Coincidentally, the same thing seems to have happened for replicas 2 and 3. Do read up on the [Metropolis criterion](http://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm) if you need more information here.\n",
    "\n",
    "Down near the bottom of the `md.log` file you can see some summary statistics where the average exchange probabilities and number of exchange events are tabulated. In a real simulation, you might see that the distribution is not very uniform despite having run for several nanoseconds, and if so you might want to adjust your temperature scheme. Check out the literature for details.\n",
    "\n",
    "The REMD statistics are accumulated only over each run, and are not stored in the `ener.edr` or `state.cpt` files, and can be lost if you use `md.log` file appending. So if you think you will want to post-process all your REMD statistics from a run in multiple stages, you will have to manage preserving all your `.log` files intact, e.g. by using `gmx_mpi mdrun -noappend` to avoid the default appending behaviour.\n",
    "\n",
    "This REMD simulation was also far too short to show anything useful.\n",
    "\n",
    "### De-multiplexing an REMD trajectory in GROMACS\n",
    "\n",
    "There are two ways the programmer could implement REMD. At exchange attempts the replica simulations could exchange coordinates, or they could exchange temperatures. Other MD packages tend to exchange temperatures; GROMACS exchanges coordinates. This means that the trajectory written to a single file belongs to the ensemble described by the matching `.tpr` file. Often this is convenient, because you only want to look at the ensemble at your lowest temperature. Because of the exchanges, that trajectory will no longer be continuous - some totally different structure will appear every time an exchange happened. This will mean the time evolution of observables like RMS deviation over time from some other structure will show abrupt jumps.\n",
    "\n",
    "There are some times you want a trajectory that has continuous coordinates, despite the \"jumps\" in ensemble space. This means chopping up each trajectory file into pieces and pasting them back together in the right order. The Perl script `demux.pl` installed with GROMACS will analyse a single REMD `.log` file and writes two special `.xvg` files that describe the transformation matrices for the trajectory time series from one form to the other, and back. `gmx_mpi trjcat -demux` can use these files to do the cut-and-paste on the trajectory files for you.\n",
    "\n",
    "## Options for other investigations\n",
    "\n",
    "- Solvated systems - many more degrees of freedom, so need more hardware to run the simulation, and more replicas to span a given temperature range\n",
    "- REST (Replica Exchange with Solute Tempering) available as a plugin to GROMACS\n",
    "\n",
    "## Conclusion\n",
    "\n",
    "Here we've scratched the surface of the possibilities of replica-exchange. There are lots of fancy ways to change the physics of the replicas from which you don't want to sample, in order to enhance the sampling at the one you do want to sample, including actually changing the Hamiltonian!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
