#!/bin/bash -l

# Allocation number
#SBATCH -A edu19.bioexcel

#SBATCH --reservation=bioexcel-2019-06-12

# Job name
#SBATCH -J my_job

# Wall-clock time
#SBATCH -t 1:00:00

# Number of nodes
#SBATCH -N 1

# Error file
#SBATCH -e job-%j.err

# Output file
#SBATCH -o job-%j.out

# Set the number of MPI ranks and OpenMP threads
nmpi_per_node=32
let nmpi=$nmpi_per_node*$SLURM_NNODES
let nomp=32/$nmpi_per_node
export OMP_NUM_THREADS=$nomp

APRUN_OPTIONS="-n $nmpi -N $nmpi_per_node -d $nomp -cc none"

NAME="run"

module swap PrgEnv-cray PrgEnv-gnu
module add gromacs/2019

# Run simulation
aprun $APRUN_OPTIONS gmx_mpi mdrun -deffnm $NAME -cpi $NAME
